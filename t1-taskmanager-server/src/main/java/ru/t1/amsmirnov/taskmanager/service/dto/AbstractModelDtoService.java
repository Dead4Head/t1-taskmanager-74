package ru.t1.amsmirnov.taskmanager.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.amsmirnov.taskmanager.api.service.dto.IAbstractDtoService;
import ru.t1.amsmirnov.taskmanager.comparator.NameComparator;
import ru.t1.amsmirnov.taskmanager.comparator.StatusComparator;
import ru.t1.amsmirnov.taskmanager.dto.model.AbstractModelDTO;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.IdEmptyException;
import ru.t1.amsmirnov.taskmanager.repository.dto.AbstractDtoRepository;

import java.util.*;

public abstract class AbstractModelDtoService<M extends AbstractModelDTO, R extends AbstractDtoRepository<M>>
        implements IAbstractDtoService<M> {

    @NotNull
    @Autowired
    protected R repository;

    public AbstractModelDtoService(@NotNull final R repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    @Transactional
    public M add(@Nullable final M model) throws AbstractException {
        if (model == null) throw new ModelNotFoundException();
        repository.save(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> addAll(@Nullable final Collection<M> models) throws AbstractException {
        if (models == null) throw new ModelNotFoundException();
        repository.saveAll(models);
        return models;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> set(@Nullable final Collection<M> models) throws AbstractException {
        if (models == null) throw new ModelNotFoundException();
        removeAll();
        addAll(models);
        return models;
    }

    @NotNull
    @Override
    public List<M> findAll() throws AbstractException {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) throws AbstractException {
        return repository.findAll();
    }

    @NotNull
    @Override
    public M findOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Optional<M> optionalM = repository.findById(id);
        if (!optionalM.isPresent()) throw new ModelNotFoundException();
        return optionalM.get();
    }

    @NotNull
    @Override
    @Transactional
    public M update(@Nullable final M model) throws AbstractException {
        if (model == null) throw new ModelNotFoundException();
        repository.save(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public M removeOne(@Nullable final M model) throws AbstractException {
        if (model == null) throw new ModelNotFoundException();
        repository.delete(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public M removeOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Optional<M> model = repository.findById(id);
        if (!model.isPresent()) throw new ModelNotFoundException();
        repository.delete(model.get());
        return model.get();
    }

    @Override
    @Transactional
    public void removeAll() {
        repository.deleteAll();
    }

    @Override
    public long getSize() throws AbstractException {
        return repository.count();
    }

    @Override
    public boolean existById(@NotNull final String id) throws AbstractException {
        return repository.existsById(id);
    }

    protected Sort getComparator(Comparator<?> comparator) {
        if (comparator instanceof StatusComparator) return Sort.by("status", "created");
        if (comparator instanceof NameComparator) return Sort.by("name", "created");
        return Sort.by("created");
    }

}
