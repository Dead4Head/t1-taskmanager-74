package ru.t1.amsmirnov.taskmanager.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class DataXmlSaveFasterXMLRequest extends AbstractUserRequest {

    public DataXmlSaveFasterXMLRequest() {
    }

    public DataXmlSaveFasterXMLRequest(@Nullable String token) {
        super(token);
    }

}
