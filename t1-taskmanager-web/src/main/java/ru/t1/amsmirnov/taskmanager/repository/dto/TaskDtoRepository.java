package ru.t1.amsmirnov.taskmanager.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.TaskWebDto;

import java.util.List;

public interface TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskWebDto> {

    @NotNull
    List<TaskWebDto> findAllByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

    void deleteByUserIdAndProjectId(@NotNull final String userId, final @NotNull String projectId);

}
